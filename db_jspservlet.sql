create database java_01_shop;

use java_01_shop;

# 1 quyen co nhieu user
create table `role`(
	id int primary key not null auto_increment,
    name varchar(100) not null
);

create table `user`(
	id int(11) primary key not null auto_increment,
    user_name varchar(100) not null,
    password varchar(50) not null,
    phone varchar(11) not null,
    email varchar(100) not null,
    role_id int,
    constraint fk_user_role foreign key (role_id) references role(id)
);