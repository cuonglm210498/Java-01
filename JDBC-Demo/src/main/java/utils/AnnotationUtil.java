package utils;

import annotation.Column;
import annotation.Entity;
import annotation.Id;

public class AnnotationUtil {

    public static String getTableName(Class<?> clazz){
        return clazz.getDeclaredAnnotation(Entity.class).name();
    }

    public static String getPrimaryKey(Class<?> tClass, String fieldName) throws NoSuchFieldException {
        return tClass.getDeclaredField(fieldName).getDeclaredAnnotation(Id.class).value();
    }

    public static String getFieldName(Class<?> tClass, String fieldName) throws NoSuchFieldException {
        return tClass.getDeclaredField(fieldName).getDeclaredAnnotation(Column.class).value();
    }

}
