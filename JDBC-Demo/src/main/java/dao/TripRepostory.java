package dao;

import model.Trip;

import java.util.List;

public interface TripRepostory extends JpaRepository<Trip, String> {

}
