package dao.impl;

import dao.TripRepostory;
import model.Trip;

public class TripRepositoryImpl extends BaseQuery<Trip, String> implements TripRepostory {

//    @Override
//    public void insert(Trip trip) {
//
//        Connection connection = MySQLConnectionUtil.getConnection();
//        String sql = "INSERT INTO ChuyenBay(MaCB, GaDi, GaDen, DoDai, GioDi, GioDen, ChiPhi) VALUES (?,?,?,?,?,?,?)";
//
//        try {
//            connection.setAutoCommit(false);
//
//            PreparedStatement preparedStatement = connection.prepareStatement(sql);
//
//            preparedStatement.setString(1, trip.getId());
//            preparedStatement.setString(2,trip.getStart());
//            preparedStatement.setString(3, trip.getDestination());
//            preparedStatement.setLong(4, trip.getLength());
//            preparedStatement.setTime(5, Time.valueOf(trip.getStartTime()));
//            preparedStatement.setTime(6, Time.valueOf(trip.getDestinationTime()));
//            preparedStatement.setInt(7, trip.getPrice());
//
//            preparedStatement.executeUpdate();
//            connection.commit();
//        } catch (SQLException throwables) {
//            try {
//                connection.rollback();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }finally {
//            MySQLConnectionUtil.disConnection(connection);
//        }
//    }
//
//    @Override
//    public void update(String id, Trip trip) {
//
//        String sql = "UPDATE ChuyenBay SET GaDi = ?, GaDen = ?, DoDai = ?, GioDi = ?, GioDen = ?, ChiPhi = ? WHERE MaCB = ?";
//
//        Connection connection = MySQLConnectionUtil.getConnection();
//
//        try {
//            connection.setAutoCommit(false);
//            PreparedStatement ps = connection.prepareStatement(sql);
//
//            ps.setString(1, trip.getStart());
//            ps.setString(2, trip.getDestination());
//            ps.setLong(3, trip.getLength());
//            ps.setTime(4, Time.valueOf(trip.getStartTime()));
//            ps.setTime(5, Time.valueOf(trip.getDestinationTime()));
//            ps.setInt(6, trip.getPrice());
//            ps.setString(7, id);
//
//            ps.executeUpdate();
//            connection.commit();
//        } catch (SQLException throwables) {
//            try {
//                connection.rollback();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }finally {
//            MySQLConnectionUtil.disConnection(connection);
//        }
//    }
//
//    @Override
//    public void delete(String id) {
//
//        String sql = "DELETE FROM ChuyenBay WHERE MaCB = ?";
//
//        Connection connection = MySQLConnectionUtil.getConnection();
//
//        try {
//            connection.setAutoCommit(false);
//
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setString(1, id);
//
//            ps.executeUpdate();
//            connection.commit();
//        } catch (SQLException throwables) {
//            try {
//                connection.rollback();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }finally {
//            MySQLConnectionUtil.disConnection(connection);
//        }
//
//
//    }
//
//    @Override
//    public Trip findById(String id) {
//
//        String sql = "SELECT * FROM ChuyenBay WHERE MaCB = ?";
//
//        try(Connection connection = MySQLConnectionUtil.getConnection()){
//
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setString(1, id);
//
//            ResultSet rs = ps.executeQuery();
//
//            Trip trip = new Trip();
//
//            while (rs.next()){
//                trip = convertToTrip(rs);
//            }
//
//            return trip;
//        }catch (SQLException e){
//            return null;
//        }
//    }
//
//    @Override
//    public List<Trip> findAll() {
//
//        String sql = "SELECT * FROM ChuyenBay";
//
//        try(Connection connection = MySQLConnectionUtil.getConnection()){
//            PreparedStatement ps = connection.prepareStatement(sql);
//
//            ResultSet rs = ps.executeQuery();
//
//            List<Trip> trips = new ArrayList<>();
//
//            while (rs.next()){
//                Trip trip = convertToTrip(rs);
//                trips.add(trip);
//            }
//            return trips;
//        } catch (SQLException throwables) {
//            return null;
//        }
//    }
//
//    public Trip convertToTrip(ResultSet rs) throws SQLException {
//        Trip trip = new Trip();
//        trip.setId(rs.getString("MaCB"));
//        trip.setStart(rs.getString("GaDi"));
//        trip.setDestination(rs.getString("GaDen"));
//        trip.setLength(rs.getLong("DoDai"));
//        trip.setStartTime(rs.getTime("GioDi").toLocalTime());
//        trip.setDestinationTime(rs.getTime("GioDen").toLocalTime());
//        trip.setPrice(rs.getInt("ChiPhi"));
//
//        return trip;
//    }
}
