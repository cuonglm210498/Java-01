package com.lecuong.repository.impl;

import com.lecuong.entity.User;
import com.lecuong.repository.UserRepository;
import com.lecuong.utils.AnnotationUtil;
import com.lecuong.utils.ObjectUlti;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserRepositoryImpl extends BaseQuery<User, Integer> implements UserRepository {

    @Override
    public Optional<User> findByUserNameAndPassword(String userName, String password) throws SQLException, IllegalAccessException, NoSuchFieldException, InstantiationException {

        String sql = "SELECT * FROM user WHERE user_name = ? AND password = ?";

        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setObject(1 ,userName);
        ps.setObject(2, password);

        ResultSet rs = ps.executeQuery();

        User user = null;

        while (rs.next()) {
            user = (User) ObjectUlti.map(tClass, rs);
        }

        return Optional.of(user);
    }
}
