package com.lecuong.repository;

import com.lecuong.paging.impl.PageRequest;

import java.sql.SQLException;
import java.util.List;

public interface JpaRepository<T, ID> {

    <S extends T> S insert(T entity) throws SQLException;

    void update(T entity) throws SQLException;

    void delete(ID id) throws NoSuchFieldException, SQLException;

    <S extends T> S findById(ID id);

    <S extends T> List<S> findAll() throws SQLException, IllegalAccessException, NoSuchFieldException, InstantiationException;

    long count() throws SQLException;

    <S extends T> List<S> findAll(PageRequest pageable) throws SQLException, IllegalAccessException, NoSuchFieldException, InstantiationException;
}
