package com.lecuong.converter;

import com.lecuong.entity.User;
import com.lecuong.model.request.UserRequest;

public class UserConverter {

    //chuyen doi userRequest -> userEntity
    public static User convertToEntity(UserRequest userRequest) {

        User user = new User();
        user.setUserName(userRequest.getUserName());
        user.setPassword(userRequest.getPassword());
        user.setEmail(userRequest.getEmail());
        user.setPhone(userRequest.getPhone());
        user.setRoleId(userRequest.getRoleId());

        return user;
    }

}
